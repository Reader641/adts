#include <iostream>
#include "List.h"

using namespace std;

int main()
{
	List L1, L2; //Declare two list objects, L1 and L2


	cout << "Welcome to my List ADT client"<<endl;

	//Do some stuff with L1, L2, ... (Eg. cout<<L1.size(); )
	// ...
	cout << L1.size()<<endl;
	cout << L2.size()<<endl;
	L1.insert(8,1);
	
	for (int i = 1; i <= 10; i++)
	{
		L2.insert(i-1,i);
	}
	
	cout << L1.size()<<endl;
	cout << L2.size()<<endl;
	
	for (int i = 1; i <= 10; i++)
	{
		L1.insert(i-1,i);
	}
	
	cout << L1.size()<<endl;
	cout << L2.size()<<endl;
	cout << L2.get(1)<<endl;
	cout << L2.get(4)<<endl;
	cout << L2.get(10)<<endl;
	L2.clear();
	cout << L2.size()<<endl;
	L2.insert(1,1);
	cout << L2.get(1)<<endl;	
	cout << L2.size()<<endl;
	return 0;
}
