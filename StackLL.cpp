#include "StackLL.h"
#include <iostream>
#include <stdexcept>//used to be able to "throw" exceptions


using namespace std;

class Stack::Node	//self-referential class
{
	public:
		int data = 0;
		Node* link = nullptr;

};

Stack::~Stack()
{
	while(num_elements > 0)
	pop();
}

int Stack::size()
{
	return num_elements; 
}

void Stack::push(int val)
{
	Node* newPtr = new Node{val};
	newPtr->link = topPtr;
	topPtr = newPtr;
	num_elements++;
}

void Stack::pop()
{
	if (num_elements == 0)//if the stack is empty 
	{ 
		throw out_of_range("Stack::pop() failed. (The Stack is empty)"); //throw an "out_of_range" exception
	}
	
	Node* popPtr = topPtr;
	topPtr = topPtr->link;
	delete popPtr;
	num_elements--;
}

int Stack::top()
{
	return topPtr->data;
}

void Stack::clear()
{
	Node* clearPtr;
	while(num_elements > 0)
	{
		clearPtr = topPtr;
		topPtr = topPtr->link;
		delete clearPtr;
		num_elements--;
	}
}
